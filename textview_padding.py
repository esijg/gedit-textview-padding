from gi.repository import Gdk, Gedit, GObject, Gtk, Pango

class TextviewPadding(GObject.Object, Gedit.WindowActivatable):
	__gtype_name__ = "TextviewPadding"

	window = GObject.property(type=Gedit.Window)

	def __init__(self):
		GObject.Object.__init__(self)
		self.margins = 24

	def do_activate(self):
		self.handlers = [
			self.window.connect("window-state-event", self.on_state_changed),
			self.window.connect("tab-added", self.on_tab_added)]

	def do_deactivate(self):
		for handler in self.handlers:
			self.window.disconnect(handler)
		if self.margins > 0:
			self.margins = 0
			self.set_all_margins()

	def on_state_changed(self, win, state):
		self.set_all_margins()

	def on_tab_added(self, win, tab):
		if self.margins > 0:
			self.set_margins(tab.get_view())


	def set_all_margins(self):
		for view in self.window.get_views():
			self.set_margins(view)

	def set_margins(self, view):
		margins = self.margins
		view.set_left_margin(margins)
		view.set_right_margin(margins)
		view.set_top_margin(margins)
		view.set_bottom_margin(margins)
